// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var app = angular.module('starter', ['ionic','starter.controllers','ionicLazyLoad','ionic.rating','ngCordova']);


app.run(function($ionicPlatform,$ionicPopup,$cordovaNetwork,$state, $rootScope,$cordovaPush, $cordovaDialogs, $cordovaMedia, $cordovaToast, $http) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

      if(window.Connection) {
          if(navigator.connection.type == Connection.NONE) {
              $ionicPopup.confirm({
                  title: "No hay red",
                  content: "No tiene ninguna red habilitada en su celular."
              })
                  .then(function(result) {
                          ionic.Platform.exitApp();

                  });
          }
      }

          register();


  });
    function register() {
        var config = null;

        if (ionic.Platform.isAndroid()) {
            config = {
                "senderID": "471730096567" // REPLACE THIS WITH YOURS FROM GCM CONSOLE - also in the project URL like: https://console.developers.google.com/project/434205989073
            };
        }
        else if (ionic.Platform.isIOS()) {
            config = {
                "badge": "true",
                "sound": "true",
                "alert": "true"
            }
        }

        $cordovaPush.register(config).then(function (result) {
            console.log("Register success " + result);

            //$cordovaToast.showShortCenter('Registered for push notifications');
           // $scope.registerDisabled=true;
            // ** NOTE: Android regid result comes back in the pushNotificationReceived, only iOS returned here
            if (ionic.Platform.isIOS()) {
                //$scope.regId = result;
                storeDeviceToken("ios",result);
            }
        }, function (err) {
            console.log("Register error " + err)
        });
    }
    $rootScope.$on('$cordovaPush:notificationReceived', function (event, notification) {
        console.log(JSON.stringify([notification]));
        if (ionic.Platform.isAndroid()) {
            handleAndroid(notification);
        }
        else if (ionic.Platform.isIOS()) {
            handleIOS(notification);
        }
    });
    function handleAndroid(notification) {
        // ** NOTE: ** You could add code for when app is in foreground or not, or coming from coldstart here too
        //             via the console fields as shown.
        console.log("In foreground " + notification.foreground  + " Coldstart " + notification.coldstart);
        if (notification.event == "registered") {
            ///window.localStorage.setItem("idGCM" , notification.regid);
            storeDeviceToken("android",notification.regid);
        }
        else if (notification.event == "message") {


            $cordovaDialogs.alert(notification.message, "Ofertas Restaurantes");

        }
        else if (notification.event == "error")
            $cordovaDialogs.alert(notification.msg, "Push notification error event");
        else $cordovaDialogs.alert(notification.event, "Push notification handler - Unprocessed Event");
    }
    function handleIOS(notification) {
        // The app was already open but we'll still show the alert and sound the tone received this way. If you didn't check
        // for foreground here it would make a sound twice, once when received in background and upon opening it from clicking
        // the notification when this code runs (weird).
        if (notification.foreground == "1") {
            // Play custom audio if a sound specified.
            if (notification.sound) {
                var mediaSrc = $cordovaMedia.newMedia(notification.sound);
                mediaSrc.promise.then($cordovaMedia.play(mediaSrc.media));
            }

            if (notification.body && notification.messageFrom) {
                $cordovaDialogs.alert(notification.body, notification.messageFrom);
            }
            else $cordovaDialogs.alert(notification.alert, "Push Notification Received");

            if (notification.badge) {
                $cordovaPush.setBadgeNumber(notification.badge).then(function (result) {
                    console.log("Set badge success " + result)
                }, function (err) {
                    console.log("Set badge error " + err)
                });
            }
        }
        // Otherwise it was received in the background and reopened from the push notification. Badge is automatically cleared
        // in this case. You probably wouldn't be displaying anything at this point, this is here to show that you can process
        // the data in this situation.
        else {
            if (notification.body && notification.messageFrom) {
                $cordovaDialogs.alert(notification.body, "(RECEIVED WHEN APP IN BACKGROUND) " + notification.messageFrom);
            }
            else $cordovaDialogs.alert(notification.alert, "(RECEIVED WHEN APP IN BACKGROUND) Push Notification Received");
        }
    }
    function storeDeviceToken(type,token) {
        // Create a random userid to store with it
        var storeToken = { type: type, token: token };
        console.log("Post token for registered device with data " + JSON.stringify(storeToken));
        window.localStorage.setItem("idGCM",JSON.stringify(storeToken));
        if (window.localStorage.hasOwnProperty("userObject")){
            var user  = angular.fromJson( window.localStorage.getItem("userObject"));
            console.log(user);
            if (user.idCel != token){
                user.idCel = token;
                $http.post("http://45.40.135.73:7000/api/miembro",user);
            }


        }


        //console.log($scope.regId);
        /*
         $http.post('http://192.168.1.16:8000/subscribe', JSON.stringify(user))
         .success(function (data, status) {
         console.log("Token stored, device is successfully subscribed to receive push notifications.");
         })
         .error(function (data, status) {
         console.log("Error storing device token." + data + " " + status)
         }
         );*/
    }
});


app.config(function($stateProvider, $urlRouterProvider) {

    if(window.localStorage.hasOwnProperty("access_token") === true)
    {
        $urlRouterProvider.otherwise('/app/restaurantes');
        //$state.go('app.ofertascercanas');
    }
    else
    {
        $urlRouterProvider.otherwise('/login');
    }

  $stateProvider
   .state('login', {
    url: "/login",
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl'

  })
  .state('app', {
    url: "/app",
    abstract: true,
    cache : false,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })
  .state('app.restaurantes', {
    url: "/restaurantes",
          cache : false,
    views: {
      'menuContent': {
        templateUrl: "templates/restaurantes.html",
        controller :'RestauratesCtrl'

      }
    }
  })
  .state('app.recomendaciones', {
    url: "/recomendaciones",
    views: {
      'menuContent': {
        templateUrl: "templates/recomendaciones.html"
      }
    }
  })
  .state('app.topCasual', {
    url: "/topCasual",
    views: {
      'menuContent': {
        templateUrl: "templates/topcasual.html",
          controller :'TopCasualCtrl'
      }
    }
  }).state('app.topgourmet', {
          url: "/topgourmet",
          views: {
              'menuContent': {
                  templateUrl: "templates/topgourmet.html",
                  controller :'TopGourmetCtrl'
              }
          }
      })
      .state('app.orden', {
          url: "/orden",
          cache: false,
          views: {
              'menuContent': {
                  templateUrl: "templates/orden.html",
                  controller :'OrdenCtrl'
              }
          }
      })    .state('app.enviarPedido', {
          url: "/enviarPedido",
          cache: false,
          params: {factura : null},
          views: {
              'menuContent': {
                  templateUrl: "templates/enviarPedido.html",
                  controller :'EnviarPedidoCtrl'

              }
          }
      }).state('app.map', {
    url: "/map/lat/:lat/lon/:lon",
    views: {
      'menuContent': {
        templateUrl: "templates/map.html",
        controller :'MapCtrl',
                              resolve: {
                  currentLocation: function($q,$ionicLoading) {
                      var q = $q.defer();
                    $ionicLoading.show({
          content: 'Cargando mapa',
          showBackdrop: true
        });
                      navigator.geolocation.getCurrentPosition(function(pos) {
                          console.log('Position=')
                          console.log(pos);
                          latLong =  { 'lat' : pos.coords.latitude, 'long' : pos.coords.longitude };
                           $ionicLoading.hide();
                          q.resolve(latLong);

                      }, function(error) {

                          latLong = null;
                          $ionicLoading.hide();
                          q.reject('Failed to Get Lat Long');


                      });

                      return q.promise;
                  }
                }


      }
    }
  }).state('app.usuario', {
    url: "/usuario",
    views: {
      'menuContent': {
        templateUrl: "templates/usuario.html",
          controller : "UsuarioCtrl"
      }
    }
  }).state('app.single', {
    url: "/restaurantes/:restauranteId",
    views: {
      'menuContent': {
        templateUrl: "templates/restaurante.html",
        controller: 'RestaurateCtrl'
      }
    }
  }).state('app.ofertasDiarias',{
          url : "/ofertasDiarias",
          cache: false,

          views: {
              'menuContent' : {
                  templateUrl : "templates/tab_ofertas_diarias.html",
                  controller : "OfertasDiariasCtrl"
              }
          }
      })

    .state('app.ofertascercanas',{
        url : "/ofertascercanas",
          cache: false,
        views: {
            'menuContent' : {
                templateUrl : "templates/tab_ofertas_cercanas.html",
                controller : "OfertasCercanasCtrl"
            }
        }
    }).state('app.restaurantescercanos',{
          url : "/restaurantescercanos",
          cache : false,
          views  : {
              'menuContent' : {
                  templateUrl : "templates/restaurantes_cercanos.html",
                  controller : "RestaurantesCercanosCtrl"
              }
          }
      }).state('app.reservacion', {
        url: "/reservacion/:restauranteId",
        views: {
            'menuContent': {
                templateUrl: "templates/reservacion.html",
                controller :'ReservacionCtrl'

            }
        }
    });

});

