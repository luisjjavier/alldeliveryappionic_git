﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ofertaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitulo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescripcion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRutaImagen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRestauranteId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDias = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRestaurante = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ofertaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.ofertaBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(811, 261);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colTitulo,
            this.colDescripcion,
            this.colRutaImagen,
            this.colRestauranteId,
            this.colCreated,
            this.colDias,
            this.collat,
            this.collon,
            this.colRestaurante});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // ofertaBindingSource
            // 
            this.ofertaBindingSource.DataSource = typeof(AllDelivery.Common.Models.Oferta);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            // 
            // colTitulo
            // 
            this.colTitulo.FieldName = "Titulo";
            this.colTitulo.Name = "colTitulo";
            this.colTitulo.Visible = true;
            this.colTitulo.VisibleIndex = 1;
            // 
            // colDescripcion
            // 
            this.colDescripcion.FieldName = "Descripcion";
            this.colDescripcion.Name = "colDescripcion";
            this.colDescripcion.Visible = true;
            this.colDescripcion.VisibleIndex = 2;
            // 
            // colRutaImagen
            // 
            this.colRutaImagen.FieldName = "RutaImagen";
            this.colRutaImagen.Name = "colRutaImagen";
            this.colRutaImagen.Visible = true;
            this.colRutaImagen.VisibleIndex = 3;
            // 
            // colRestauranteId
            // 
            this.colRestauranteId.FieldName = "RestauranteId";
            this.colRestauranteId.Name = "colRestauranteId";
            this.colRestauranteId.Visible = true;
            this.colRestauranteId.VisibleIndex = 4;
            // 
            // colCreated
            // 
            this.colCreated.FieldName = "Created";
            this.colCreated.Name = "colCreated";
            this.colCreated.Visible = true;
            this.colCreated.VisibleIndex = 5;
            // 
            // colDias
            // 
            this.colDias.FieldName = "Dias";
            this.colDias.Name = "colDias";
            this.colDias.Visible = true;
            this.colDias.VisibleIndex = 6;
            // 
            // collat
            // 
            this.collat.FieldName = "lat";
            this.collat.Name = "collat";
            this.collat.Visible = true;
            this.collat.VisibleIndex = 7;
            // 
            // collon
            // 
            this.collon.FieldName = "lon";
            this.collon.Name = "collon";
            this.collon.Visible = true;
            this.collon.VisibleIndex = 8;
            // 
            // colRestaurante
            // 
            this.colRestaurante.FieldName = "Restaurante";
            this.colRestaurante.Name = "colRestaurante";
            this.colRestaurante.Visible = true;
            this.colRestaurante.VisibleIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 261);
            this.Controls.Add(this.gridControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ofertaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource ofertaBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colTitulo;
        private DevExpress.XtraGrid.Columns.GridColumn colDescripcion;
        private DevExpress.XtraGrid.Columns.GridColumn colRutaImagen;
        private DevExpress.XtraGrid.Columns.GridColumn colRestauranteId;
        private DevExpress.XtraGrid.Columns.GridColumn colCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colDias;
        private DevExpress.XtraGrid.Columns.GridColumn collat;
        private DevExpress.XtraGrid.Columns.GridColumn collon;
        private DevExpress.XtraGrid.Columns.GridColumn colRestaurante;
    }
}

