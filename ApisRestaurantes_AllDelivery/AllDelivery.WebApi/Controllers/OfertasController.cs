﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AllDelivery.Common;
using AllDelivery.Library.Helpers;
namespace AllDelivery.WebApi.Controllers
{
    public class OfertasController : BaseController
    {
        // GET api/ofertas
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/ofertas/l
        public List<AllDelivery.Common.Models.Oferta> Get(double lat , double lon) 
        {
            OfertasHelper ofertas = new OfertasHelper(Connection);
            return ofertas.GetOfertasCercanas(lat, lon);
        }

        // POST api/ofertas
        public void Post([FromBody]string value)
        {

        }

        // PUT api/ofertas/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/ofertas/5
        public void Delete(int id)
        {
        }
    }
}
