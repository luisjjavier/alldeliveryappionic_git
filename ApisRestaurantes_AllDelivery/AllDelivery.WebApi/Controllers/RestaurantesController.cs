﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AllDelivery.Library.Helpers;
using AllDelivery.Common.Models;
namespace AllDelivery.WebApi.Controllers
{
    public class RestaurantesController : BaseController
    {
        // GET api/restaurantes
        public IEnumerable<RestaurantesHeader> Get(string rows)
        {

            RestauranteHelpers restaurantes = new RestauranteHelpers(Connection);
            return restaurantes.GetAllRestaurantes2(Convert.ToInt32(rows));
        }
        public IEnumerable<RestaurantesHeader> Get()
        {
            RestauranteHelpers restaurantes = new RestauranteHelpers(Connection);
            return restaurantes.GetTopForRating();
        }
        public IEnumerable<RestaurantesHeader> Get(string Descripcion, decimal preciomenor, decimal preciomayor,double lat, double lon, int kms)
        {
            RestauranteHelpers restaurantes = new RestauranteHelpers(Connection);
            return restaurantes.GetBusqueda(Descripcion, preciomenor, preciomayor,lat,lon,kms);
        }
        public IEnumerable<RestaurantesHeader> Get(int top, int type) {

            RestauranteHelpers restaurantes = new RestauranteHelpers(Connection);
            return restaurantes.GetTopForRatingAndType(top,type);
        }
        // GET api/restaurantes/5
        public Restaurante Get(int id)
        {
             RestauranteHelpers restaurantes = new RestauranteHelpers(Connection);
            Restaurante res = restaurantes.GetRestauranteById(id);
            OfertasHelper of = new OfertasHelper (Connection);
            res.Oferta = of.GetOfertasByRestaurante(res.ID);
            res.Foto = restaurantes.GetImageRestaurantes(res.ID);
            return res;

        }



        // POST api/restaurantes
        public void Post([FromBody]string value)
        {
        }

        // PUT api/restaurantes/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/restaurantes/5
        public void Delete(int id)
        {
        }
    }
}
