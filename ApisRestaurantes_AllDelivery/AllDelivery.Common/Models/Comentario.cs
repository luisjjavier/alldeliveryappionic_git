﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Comentario
    {
        public long Id { get; set; }
        public DateTime Fecha { get; set; }
        public string Cuerpo { get; set; }
        public long IdMiembro { get; set; }
        public long IdEstadoComentario { get; set; }
        public long IdRestaurante { get; set; }
        public string Titulo { get; set; }

        public Miembro Miembro { get; set; }
        public EstadoComentario EstadoComentario { get; set; }
        public virtual Restaurante Restaurante { get; set; }
    }
}
