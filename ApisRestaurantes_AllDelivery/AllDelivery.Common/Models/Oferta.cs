﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Oferta
    {
        public long Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string RutaImagen { get; set; }
        public long RestauranteId { get; set; }
        public DateTime Created { get; set; }
        public string Dias { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public Restaurante Restaurante { get; set; }
        public List<OfertasDias> OfertasDias { get; set; }
    }
}
