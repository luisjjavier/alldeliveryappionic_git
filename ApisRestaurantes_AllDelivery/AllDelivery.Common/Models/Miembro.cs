﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Miembro
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public long IdTipoMiembro { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Direccion { get; set; }
        public bool Newsletter { get; set; }
        public string Sexo { get; set; }
        public long IdOcupacion { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public long CodigoSeguridad { get; set; }
        public int Estado { get; set; }
        public string NombreCompleto { get; set; }
        public string FbUid { get; set; }
        public string FbAccessToken { get; set; }

        public List<Comentario> Comentario { get; set; }
        public TipoMienbro TipoMienbro { get; set; }
        public Ocupacione Ocupacione { get; set; }
        public List<MiembroTipoComida> MiembroTipoComida { get; set; }
        public List<Newsletter> NewsletterList { get; set; }
        public List<Reservacione> reservaciones { get; set; }
        public List<Votaciones> Votaciones { get; set; }
    }
}
