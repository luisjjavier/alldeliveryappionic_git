﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class CaracteristicasRestaurantes
    {
        public long Id { get; set; }
        public long IdCaracteristica { get; set; }
        public long IdRestaurante { get; set; }
        public bool Value { get; set; }
        public string ValueText { get; set; }
        public int ValueInt { get; set; }

        public Caracteristica Caracteristica { get; set; }
        public Restaurante Restaurante { get; set; } 
    }
}
