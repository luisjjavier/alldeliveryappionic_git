﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class SucursalIdiomas
    {
        public long Id { get; set; }
        public long IdSucursal { get; set; }
        public int IdIdioma { get; set; }
        public string Horario { get; set; }

        public Idioma Idioma { get; set; }
        public Sucursal Sucursal { get; set; }
    }
}
