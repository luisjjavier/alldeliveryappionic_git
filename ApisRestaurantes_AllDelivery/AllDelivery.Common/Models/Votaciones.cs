﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Votaciones
    {
        public long Id { get; set; }
        public short Valor { get; set; }
        public long IdMiembro { get; set; }
        public long IdCategoria { get; set; }
        public long IdRestaurante { get; set; }
        public DateTime UltimaFecha { get; set; }

        public Categoria Categoria { get; set; }
        public Miembro Miembro { get; set; }
        public Restaurante Restaurante { get; set; }
    }
}
