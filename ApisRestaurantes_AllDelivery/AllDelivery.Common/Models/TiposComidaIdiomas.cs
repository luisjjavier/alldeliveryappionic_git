﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
   public class TiposComidaIdiomas
    {
        public int Id { get; set; }
        public int IdIdioma { get; set; }
        public long IdTiposComidas { get; set; }
        public string Descripcion { get; set; }

        public Idioma idioma { get; set; }
        public TipoComida TipoComida { get; set; }
    }
}
