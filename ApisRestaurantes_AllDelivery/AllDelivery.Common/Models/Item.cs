﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Item
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public Decimal Precio { get; set; }
        public long IdCategoriaMenu { get; set; }
        public string Descripcion { get; set; }

        public CategoriaMenu CategoriaMenu { get; set; }
        public List<ItemsIdiomas> ItemsIdiomas { get; set; } 
    }
}
