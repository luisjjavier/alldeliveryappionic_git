﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Newsletter
    {
        public long Id { get; set; }
        public string Asunto { get; set; }
        public string Cuerpo { get; set; }
        public long IdMiembro { get; set; }
        public DateTime Fecha { get; set; }

        public Miembro Miembro { get; set; }
        public List<NewsletterTipoComida> NewsletterTipoComida { get; set; } 
    }
}
