﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
   public class EstadoComentario
    {
        public long Id { get; set; }
        public string Descripcion { get; set; }

        public virtual List<Comentario> Comentario { get; set; }
    }
}
