﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Sucursal
    {
        public long Id { get; set; }
        public long IdCiudad { get; set; }
        public string Direccion { get; set; }
        public long IdRestaurante { get; set; }
        public string Telefono { get; set; }
        public long IdEstadoSucursal { get; set; }
        public int CapacidadPersonas { get; set; }
        public Double Latitud { get; set; }
        public Double Longitud { get; set; }

        public Ciudad Ciudad { get; set; }
        public List<Reservacione> reservaciones { get; set; }
        public Restaurante restaurante { get; set; }
        public List<SucursalIdiomas> SucursalIdiomas { get; set; }
    }
}
