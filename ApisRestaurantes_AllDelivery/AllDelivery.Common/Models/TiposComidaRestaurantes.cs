﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class TiposComidaRestaurantes
    {
        public long Id { get; set; }
        public long Id_tipo_comida { get; set; }
        public long Id_restaurante { get; set; }
        public long Value { get; set; }

        public Restaurante Restaurante { get; set; }
        public virtual TipoComida TipoComida { get; set; } 
    }
}
