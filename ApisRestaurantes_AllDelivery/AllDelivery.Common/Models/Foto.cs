﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Foto
    {
        public long Id { get; set; }
        public string Url { get; set; }
        public long IdRestaurante { get; set; } 
        public string Comentario { get; set; }

        public Restaurante Restaurante { get; set; }
    }
}
