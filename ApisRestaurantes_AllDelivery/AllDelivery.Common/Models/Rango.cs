﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Rango
    {
        public long Id { get; set; }
        public string Descripcion { get; set; }
        public float RangoMayor { get; set; }
        public float RangoMenor { get; set; }

        public List<Restaurante> Restaurante { get; set; }
    }
}
