﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Restaurante
    {
        public long ID { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Mapa { get; set; }
        public string Email { get; set; }
        public string Chef { get; set; }
        public long IdRangoPrecio { get; set; }
        public string Horario { get; set; }
        public bool Reservacion { get; set; }
        public long IdEstado { get; set; }
        public string Logo { get; set; }
        public string MenuPdf { get; set; }
        public long IdMenu { get; set; }
        public string Responsable { get; set; }
        public string PaginaWeb { get; set; }
        public string Descripcion { get; set; }
        public string Fax { get; set; }
        public string Infomercial { get; set; }
        public string TourVirtual { get; set; }
        public int IdTipoRestaurante { get; set; }
        public decimal OverallRating { get; set; }
        public decimal ValueRating { get; set; }
        public decimal CAT1 { get; set; }
        public decimal CAT2 { get; set; }
        public decimal CAT3 { get; set; }
        public decimal CAT4 { get; set; }
        public decimal CAT5 { get; set; }
        public int CantidadVotos { get; set; }
        public string Comidas { get; set; }
        public int SucursalId { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string GooglePlus { get; set; }
        public string Foursquare { get; set; }


       // public List<CaracteristicasRestaurantes> CaracteristicasRestaurantes { get; set; }
        public List<Comentario> Comentario { get; set; }
        public List<Foto> Foto { get; set; }
        public Menu Menu { get; set; }
        public List<Oferta> Oferta { get; set; }
        public Rango Rango { get; set; }
        public List<Votaciones> Votaciones { get; set; }
        //public List<RestauranteIdioma> RestauranteIdioma { get; set; }
        public TipoRestaurante TipoRestaurante { get; set; }
        public List<Sucursal> Sucursal { get; set; }
       // public List<TiposComidaRestaurantes> TiposComidaRestaurantes { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public string direccion { get; set; }
    }
}
