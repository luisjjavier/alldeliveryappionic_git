﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class RestauranteIdioma
    {
        public int Id { get; set; }
        public long IdRestaurante { get; set; }
        public int IdIdioma { get; set; }
        public string Horario { get; set; }
        public string Descripcion { get; set; }

        public Idioma Idioma { get; set; }
        public Restaurante Restaurante { get; set; }
    }
}
