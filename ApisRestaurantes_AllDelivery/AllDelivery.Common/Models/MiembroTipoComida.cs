﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class MiembroTipoComida
    {

        public long Id { get; set; }
        public long IdMiembro { get; set; }
        public long IdTipoComida { get; set; }
        public long Value { get; set; }

        public Miembro Miembro { get; set; }
        public TipoComida TipoComida { get; set; }
    }
}
