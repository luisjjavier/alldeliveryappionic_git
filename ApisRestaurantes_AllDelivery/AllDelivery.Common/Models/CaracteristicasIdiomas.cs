﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class CaracteristicasIdiomas
    {
        public int Id { get; set; }
        public long IdCaracteristica { get; set; }
        public int IdIdioma { get; set; }
        public string Descripcion { get; set; }

        public virtual Caracteristica Caracteristica { get; set; }
        public virtual Idioma Idioma { get; set; } 
    }
}
