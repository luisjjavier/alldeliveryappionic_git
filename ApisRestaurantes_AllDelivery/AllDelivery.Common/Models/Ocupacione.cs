﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Ocupacione
    {
        public long Id { get; set; }
        public string Descripcion { get; set; }

        public List<Miembro> Miembro { get; set; }
        public List<OcupacionesIdioma> OcupacionesIdioma { get; set; }
    }
}
