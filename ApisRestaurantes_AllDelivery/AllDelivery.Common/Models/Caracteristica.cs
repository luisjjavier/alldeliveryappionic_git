﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Caracteristica
    {
        public long Id { get; set; }
        public string Descripcion { get; set; }
        public string HtmlId { get; set; }

        public List<CaracteristicasIdiomas> CaracteristicasIdiomas { get; set; }
        public List<CaracteristicasRestaurantes> CaracteristicasRestaurantes { get; set; }
    }
}
