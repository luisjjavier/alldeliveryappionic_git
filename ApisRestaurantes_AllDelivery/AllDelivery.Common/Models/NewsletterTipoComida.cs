﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class NewsletterTipoComida
    {
        public long Id { get; set; }
        public long IdNewsletter { get; set; }
        public long IdTipoComida { get; set; }

        public virtual Newsletter Newsletter { get; set; }
        public virtual TipoComida TipoComida { get; set; }  
    }
}
