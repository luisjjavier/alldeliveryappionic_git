﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
   public class TipoComida
    {
        public long Id { get; set; }
        public string Descripcion { get; set; }

        public List<MiembroTipoComida> MiembroTipoComida { get; set; }
        public List<NewsletterTipoComida> NewsletterTipoComida { get; set; }
        public List<TiposComidaRestaurantes> TiposComidaRestaurantes { get; set; }
        public List<TiposComidaIdiomas> TiposComidaIdiomas { get; set; }
    }
}
