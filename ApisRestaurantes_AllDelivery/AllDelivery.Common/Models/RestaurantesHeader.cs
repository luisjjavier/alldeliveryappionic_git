﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
   public  class RestaurantesHeader
    {
        public long ID { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Logo { get; set; }
        public decimal OverallRating { get; set; }
        public decimal ValueRating { get; set; }
    }
}
