﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Idioma
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string IsoCode { get; set; }

        public List<CaracteristicasIdiomas> CaracteristicasIdiomas { get; set; }
        public List<CategoriasIdiomas> CategoriasIdiomas { get; set; }
        public List<CategoriasMenuIdiomas> CategoriasMenuIdiomas { get; set; }
        public List<ItemsIdiomas> ItemsIdiomas { get; set; }
        public List<MenusIdiomas> MenusIdiomas { get; set; }
        public List<OcupacionesIdioma> OcupacionesIdioma { get; set; }
        public List<RestauranteIdioma> RestauranteIdioma { get; set; }
        public List<SucursalIdiomas> SucursalIdiomas { get; set; }
        public List<TiposComidaIdiomas> TiposComidaIdiomas { get; set; }
    }
}
