﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class OfertasDias
    {
        public long Id { get; set; }
        public byte Weekday { get; set; }
        public long IdOferta { get; set; }

        public Oferta Oferta { get; set; }
    }
}
