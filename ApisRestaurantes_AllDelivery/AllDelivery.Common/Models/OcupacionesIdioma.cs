﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class OcupacionesIdioma
    {
        public int Id { get; set; }
        public long IdOcupacion { get; set; }
        public int IdIdioma { get; set; }
        public string Descripcion { get; set; }

        public Idioma Idioma { get; set; }
        public Ocupacione Ocupacione { get; set; }
    }
}
