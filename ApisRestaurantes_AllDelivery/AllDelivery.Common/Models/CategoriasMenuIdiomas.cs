﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class CategoriasMenuIdiomas
    {
        public int Id { get; set; }
        public long IdCategoriaMenu { get; set; }
        public int IdIdioma { get; set; }
        public string Descripcion { get; set; }

        public CategoriaMenu CategoriaMenu { get; set; } 
        public Idioma Idioma { get; set; }
    }
}
