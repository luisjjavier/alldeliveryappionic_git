﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Menu
    {
        public long Id { get; set; }
        public string Descripcion { get; set; }
        public sbyte Publicado { get; set; }

        public List<CategoriaMenu> CategoriaMenu { get; set; }
        public List<MenusIdiomas> MenusIdiomas { get; set; }
        public List<Restaurante> Restaurante { get; set; }
    }
}
