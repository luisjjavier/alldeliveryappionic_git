﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Reservacione
    {
        public long Id { get; set; }
        public DateTime Fecha { get; set; }
        public long IdMiembro { get; set; }
        public long IdEstadoReservacion { get; set; }
        public int CantidadPersonas { get; set; }
        public bool Fumador { get; set; }
        public long IdSucursal { get; set; }
        public string EstadoReservacion { get; set; }

        public Miembro miembro { get; set; }
        public Sucursal Sucursal { get; set; }
    }
}
