﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class CategoriaMenu
    {
        public long Id { get; set; }
        public string Descripcion { get; set; }
        public long IdMenu { get; set; }

        public Menu Menu { get; set; }
        public List<CategoriasMenuIdiomas> CategoriasMenuIdiomas { get; set; }
        public List<Item> Item { get; set; }
    }
}
