﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class GeneryItem
    {
        public int itemID { get; set; }
        public String NameItem { get; set; }
        public int TypeItem { get; set; }
    }
}
