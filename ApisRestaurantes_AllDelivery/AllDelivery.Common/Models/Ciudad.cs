﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class Ciudad
    {
        public long id { get; set; }
        public string descripcion { get; set; }

        public List<Sucursal> Sucursal { get; set; }
    }
}
