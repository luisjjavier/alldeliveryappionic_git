﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDelivery.Common.Models
{
    public class CategoriasIdiomas
    {
        public int Id { get; set; }
        public long IdCategoria { get; set; }
        public int IdIdioma { get; set; }
        public string Descripcion { get; set; }

        public virtual Categoria categoria { get; set; }
        public Idioma idioma { get; set; }
    }
}
