﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AllDelivery.Common.Models;
namespace AllDelivery.Library.Helpers
{
    public class MenuHelper
    {
        private MySqlConnection connection;
        private bool NoCerrar;
       public MenuHelper(string con)
        {
            connection = new MySqlConnection(con);
        }

       public MenuHelper(MySqlConnection con)
       {
           connection = con;
           NoCerrar = true;
          
       }
       #region Menu
       public Menu GetMenubyId(long id)
       {
           Menu menu = new Menu();
           if (connection.State != System.Data.ConnectionState.Open)
           {
               connection.Open();
           }
           MySqlCommand cmd = new MySqlCommand("SELECT * FROM menus where id =" + id.ToString(), connection);
           MySqlDataReader menuReader = cmd.ExecuteReader();
           while (menuReader.Read())
           {
               menu.Id = Convert.ToInt32(menuReader["id"]);
               menu.Descripcion = menuReader["descripcion"].ToString();
               menu.Publicado = (sbyte)menuReader["publicado"];


           }
           menuReader.Close();
           menu.CategoriaMenu = new List<CategoriaMenu>();
           menu.CategoriaMenu = GetCategoriasMenu(menu.Id);
           if (!NoCerrar)
           {
               connection.Close();
           }
           return menu;
       }
       #endregion
        #region Categoria

       public List<CategoriaMenu> GetCategoriasMenu(long IdMenu) {
           List<CategoriaMenu> catMList = new List<CategoriaMenu>();
           CategoriaMenu catMenu;
            if (connection.State != System.Data.ConnectionState.Open)
           {
               connection.Open();
           }
           MySqlCommand cmd = new MySqlCommand("SELECT * FROM categorias_menu where id_menu =" + IdMenu.ToString(), connection);
           MySqlDataReader catReader = cmd.ExecuteReader();
           while (catReader.Read())
           {
               catMenu = new CategoriaMenu();
               catMenu.Descripcion = catReader["descripcion"].ToString();
               catMenu.Id = Convert.ToInt32(catReader["id"]);
               catMenu.IdMenu = Convert.ToInt32(catReader["id_menu"]);
              
               catMList.Add(catMenu);
           }
           catReader.Close();
           foreach (var i in catMList) {
               i.Item = new List<Item>();
               i.Item = GetItemsByCat(i.Id);
           }
           if (!NoCerrar)
           {
               connection.Close();
           }
           return catMList;
       }

       public List<Item> GetItemsByCat(long IdCat) {
           List<Item> Platos = new List<Item>();
           Item item;
           if (connection.State != System.Data.ConnectionState.Open)
           {
               connection.Open();
           }
           MySqlCommand cmd = new MySqlCommand("SELECT * FROM items where id_categoria_menu =" + IdCat.ToString(), connection);
           MySqlDataReader itemReader = cmd.ExecuteReader();
           while (itemReader.Read())
           {
               item = new Item();
               item.Descripcion = itemReader["descripcion"].ToString();
               item.Id = Convert.ToInt32(itemReader["id"]);
               item.Nombre = itemReader["nombre"].ToString();
               item.Precio = itemReader["precio"] == DBNull.Value ? 0 : Convert.ToDecimal(itemReader["precio"]);
               item.IdCategoriaMenu = IdCat;
               Platos.Add(item);
           }
           itemReader.Close();
           if (!NoCerrar)
           {
               connection.Close();
           }
           return Platos;
       }
        #endregion
    }
}
