﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AllDelivery.Common;
using AllDelivery.Common.Models;
using MySql.Data.MySqlClient;
namespace AllDelivery.Library.Helpers
{
   // 18.4280109,-69.9798975

    public class OfertasHelper
    {
        private MySqlConnection connection;
        public OfertasHelper(string con)
        {
            connection = new MySqlConnection(con);
        }
        public List<Oferta> GetOfertasCercanas(double lat, double lon)
        {
            List<Oferta> ofertas = new List<Oferta>();
            int dia = (int)DateTime.Now.DayOfWeek;
            connection.Open();
            MySqlCommand cmd = new MySqlCommand("select DISTINCT ofd.`weekday`,of.id, of.titulo, of.descripcion, of.ruta_imagen, of.dias, r.`id` as idResturan, r.`nombre` , su.latitud, su.longitud from `great_restaurantecomdo`.`ofertas_dias` ofd inner join ofertas as of on of.id = ofd.id_oferta inner join `great_restaurantecomdo`.`restaurantes` as r on of.restaurante_id = r.id inner join sucursales as su on su.id = r.`sucursal_id` WHERE ofd.`weekday` =" + dia, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            Oferta of;
            while (dataReader.Read())
            {
                of = new Oferta();
                of.Descripcion = dataReader["descripcion"].ToString();
                of.Dias = dataReader["dias"].ToString();
                of.Id = Convert.ToInt32(dataReader["id"]);
               of.RutaImagen = "http://restaurante.com.do" + dataReader["ruta_imagen"].ToString();
                try
                {
                    of.lon = dataReader["longitud"] == DBNull.Value ? 0 : Convert.ToDouble(dataReader["longitud"]);
                    of.lat = dataReader["latitud"] == DBNull.Value ? 0 : Convert.ToDouble(dataReader["latitud"]);
                }
                catch (Exception)
                {
                    of.lat = 0;
                    of.lon = 0;

                }
               
                of.Titulo = dataReader["titulo"].ToString();
                of.RestauranteId = Convert.ToInt32(dataReader["idResturan"].ToString());
                // 18.4280109,-69.9798975
                double dis = distance(lat , lon, of.lat, of.lon, 'K');
                if (dis < 50){
                    ofertas.Add(of);
                }
            }
            connection.Close();
            return ofertas;
        }

        public List<Oferta> GetOfertasByRestaurante(long resid)
        {
            List<Oferta> ofertas = new List<Oferta>();
            connection.Open();
            MySqlCommand cmd = new MySqlCommand("select DISTINCT ofd.`weekday`,of.id, of.titulo, of.descripcion, of.ruta_imagen, of.dias, r.`id` as idResturan, r.`nombre` , su.latitud, su.longitud from `great_restaurantecomdo`.`ofertas_dias` ofd inner join ofertas as of on of.id = ofd.id_oferta inner join `great_restaurantecomdo`.`restaurantes` as r on of.restaurante_id = r.id inner join sucursales as su on su.id = r.`sucursal_id` WHERE r.`id` =" + resid, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            Oferta of;
            while (dataReader.Read())
            {
                of = new Oferta();
                of.Descripcion = dataReader["descripcion"].ToString();
                of.Dias = dataReader["dias"].ToString();
                of.Id = Convert.ToInt32(dataReader["id"]);
                of.RutaImagen = "http://restaurante.com.do" + dataReader["ruta_imagen"].ToString();
                try
                {
                    of.lon = dataReader["longitud"] == DBNull.Value ? 0 : Convert.ToDouble(dataReader["longitud"]);
                    of.lat = dataReader["latitud"] == DBNull.Value ? 0 : Convert.ToDouble(dataReader["latitud"]);
                }
                catch (Exception)
                {
                    of.lat = 0;
                    of.lon = 0;

                }

                of.Titulo = dataReader["titulo"].ToString();
                of.RestauranteId = Convert.ToInt32(dataReader["idResturan"].ToString());
              
                    ofertas.Add(of);
                
            }
            connection.Close();
            return ofertas;
        }

        public const double EarthRadius = 6371;
        public double GetDistance(double lat1, double lat2, double lon1, double lon2)
        {
            double distance = 0;
            double Lat = (lat2 - lat1) * (Math.PI / 180);
            double Lon = (lon1 - lon2) * (Math.PI / 180);
            double a = Math.Sin(Lat / 2) * Math.Sin(Lat / 2) + Math.Cos(lat1 * (Math.PI / 180)) * Math.Cos(lat2 * (Math.PI / 180)) * Math.Sin(Lon / 2) * Math.Sin(Lon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            distance = EarthRadius * c;
            return distance / 1000;
        }
        private double distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {

            double theta = lon1 - lon2;

            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));

            dist = Math.Acos(dist);

            dist = rad2deg(dist);

            dist = dist * 60 * 1.1515;

            if (unit == 'K')
            {

                dist = dist * 1.609344;

            }
            else if (unit == 'N')
            {

                dist = dist * 0.8684;

            }

            return (dist);

        }

        private double deg2rad(double deg)
        {

            return (deg * Math.PI / 180.0);
        }

        private double rad2deg(double rad)
        {

            return (rad / Math.PI * 180.0);

        }

    }
}
