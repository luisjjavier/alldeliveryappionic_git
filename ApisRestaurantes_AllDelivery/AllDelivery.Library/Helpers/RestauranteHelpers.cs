﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AllDelivery.Common.Models;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Configuration;
namespace AllDelivery.Library.Helpers
{
    public  class RestauranteHelpers
    {
        private MySqlConnection connection;
        public RestauranteHelpers(string con) {
            connection = new MySqlConnection(con);
        }
        public List<Restaurante> GetAllRestaurantes() {
            List<Restaurante> lisRest = new List<Restaurante>();
            Restaurante res;
            if (!connection.Ping())
            {
                connection.Open();
            }
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM restaurantes", connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read()) {
                res = new Restaurante();
                res.ID = Convert.ToInt32 ( dataReader["id"]);
                res.Nombre = dataReader["nombre"].ToString();
                res.Telefono =dataReader["telefono"].ToString();
                res.Mapa = dataReader["mapa"].ToString();
                res.Email = dataReader["email"].ToString();
                res.Chef = dataReader["chef"].ToString();
                res.IdRangoPrecio = dataReader["id_rango_precio"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_rango_precio"].ToString());
                res.Horario = dataReader["horario"].ToString();
                res.Reservacion = dataReader["reservacion"] == DBNull.Value ? true : Convert.ToBoolean(dataReader["reservacion"].ToString());
                res.IdEstado = dataReader["id_estado"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_estado"].ToString());
                res.Logo = "http://img.restaurante.com.do/img/logos/" + dataReader["logo"].ToString();
                res.MenuPdf = dataReader["menu_pdf"].ToString();
                res.IdMenu = dataReader["id_menu"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_menu"].ToString());
                res.Responsable = dataReader["responsable"].ToString();
                res.PaginaWeb = dataReader["pagina_web"].ToString();
                res.Descripcion = dataReader["descripcion"].ToString();
              //  res.IdMenu = Convert.ToInt32(dataReader["id_menu"].ToString());
                res.IdTipoRestaurante = dataReader["id_tipo_restaurante"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_tipo_restaurante"].ToString());
                res.OverallRating = dataReader["overall_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["overall_rating"].ToString());
                res.ValueRating = dataReader["value_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["value_rating"].ToString());
                res.CAT1 = dataReader["CAT1"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT1"].ToString());
                res.CAT2 = dataReader["CAT2"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT2"].ToString());
                res.CAT3 = dataReader["CAT3"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT3"].ToString());
                res.CAT4 = dataReader["CAT4"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT4"].ToString());
                res.CAT5 = dataReader["CAT5"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT5"].ToString());
                res.CantidadVotos = dataReader["cantidad_votos"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["cantidad_votos"].ToString());
                res.Comidas = dataReader["comidas"].ToString();
                res.SucursalId = dataReader["sucursal_id"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["sucursal_id"]);
                res.Facebook = dataReader["facebook"].ToString();
                res.Twitter = dataReader["twitter"].ToString();
                res.GooglePlus =  dataReader["google_plus"].ToString();
                res.Foursquare = dataReader["foursquare"].ToString();
                lisRest.Add(res);
            }
            connection.Close();
            return lisRest;
        }
        public List<RestaurantesHeader> GetAllRestaurantes2(int rows ) {
            List<RestaurantesHeader> lisRest = new List<RestaurantesHeader>();
            RestaurantesHeader res;
            if (!connection.Ping())
            {
                connection.Open();
            }
            MySqlCommand cmd = new MySqlCommand("SELECT  * FROM restaurantes LIMIT " + rows.ToString(), connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                res = new RestaurantesHeader();
                res.ID = Convert.ToInt32(dataReader["id"]);
                res.Nombre = dataReader["nombre"].ToString();
                res.Telefono = dataReader["telefono"].ToString();
                res.Logo = "http://img.restaurante.com.do/img/logos/" + dataReader["logo"].ToString();
                res.OverallRating = dataReader["overall_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["overall_rating"].ToString());
                res.ValueRating = dataReader["value_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["value_rating"].ToString());
                lisRest.Add(res);

            }
            connection.Close();
            return lisRest;
        }
        public List<RestaurantesHeader> GetBusqueda(string Descripcion, decimal preciomenor, decimal preciomayor, double lat, double lon, int kms)
        {
            List<RestaurantesHeader> lisRest = new List<RestaurantesHeader>();
            RestaurantesHeader res;
            if (!connection.Ping())
            {
                connection.Open();
            }
            string query = "select "
                + "res.id,res.nombre,res.telefono,"
                + "res.logo,res.overall_rating,res.value_rating,"
                + "r.rango_mayor, r.rango_menor,r.descripcion"
                + " FROM restaurantes AS res "
                + " inner join menus as m "
                + " on m.id = res.id_menu "
                + " inner join categorias_menu as catM "
                + " on "
                + " m.id = catM.id_menu "
                + " INNER JOIN items AS i"
                + " ON "
                + " i.id_categoria_menu= catM.id "
                + " INNER JOIN rangos AS r "
                + " ON res.id_rango_precio = r.id "
                + " Where res.nombre like '%" + Descripcion + "%'" + "or i.nombre like '%" + Descripcion + "%'"
                + " and  r.rango_mayor  >=  " + preciomenor.ToString() + "  and  r.rango_menor <= " + preciomayor.ToString()
                + " GROUP BY res.id,res.nombre,res.telefono,res.logo,res.overall_rating,res.value_rating,r.rango_mayor, r.rango_menor,r.descripcion ";
      
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
         
            while (dataReader.Read())
            {
                res = new RestaurantesHeader();
                res.ID = Convert.ToInt32(dataReader["id"]);
                res.Nombre = dataReader["nombre"].ToString();
                res.Telefono = dataReader["telefono"].ToString();
                res.Logo = "http://img.restaurante.com.do/img/logos/" + dataReader["logo"].ToString();
                res.OverallRating = dataReader["overall_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["overall_rating"].ToString());
                res.ValueRating = dataReader["value_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["value_rating"].ToString());
                lisRest.Add(res);

            }
            connection.Close();
            return lisRest;
        }
        /// <summary>
        /// 1 top Casual
        /// 2 Gourmet
        /// </summary>
        /// <returns></returns>
        public List<RestaurantesHeader> GetTopForRating()
        {
            List<RestaurantesHeader> lisRest = new List<RestaurantesHeader>();
            RestaurantesHeader res;
            if (!connection.Ping())
            {
                connection.Open();
            }
            MySqlCommand cmd = new MySqlCommand("SELECT  * FROM restaurantes ORDER BY overall_rating DESC LIMIT 20 ", connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                res = new RestaurantesHeader();
                res.ID = Convert.ToInt32(dataReader["id"]);
                res.Nombre = dataReader["nombre"].ToString();
                res.Telefono = dataReader["telefono"].ToString();
                res.Logo = "http://img.restaurante.com.do/img/logos/" + dataReader["logo"].ToString();
                res.OverallRating = dataReader["overall_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["overall_rating"].ToString());
                res.ValueRating = dataReader["value_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["value_rating"].ToString());
                lisRest.Add(res);

            }
            connection.Close();
            return lisRest;
        }
        public List<RestaurantesHeader> GetTopForRatingAndType(int top, int type)
        {
            List<RestaurantesHeader> lisRest = new List<RestaurantesHeader>();
            RestaurantesHeader res;
            if (!connection.Ping())
            {
                connection.Open();
            }
            MySqlCommand cmd = new MySqlCommand("SELECT res.* FROM restaurantes AS res INNER JOIN tipos_restaurantes AS tpRes ON tpRes.id = res.id_tipo_restaurante WHERE tpRes.id = " + type.ToString() + " ORDER BY overall_rating DESC LIMIT  " + top.ToString(), connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                res = new RestaurantesHeader();
                res.ID = Convert.ToInt32(dataReader["id"]);
                res.Nombre = dataReader["nombre"].ToString();
                res.Telefono = dataReader["telefono"].ToString();
                res.Logo = "http://img.restaurante.com.do/img/logos/" + dataReader["logo"].ToString();
                res.OverallRating = dataReader["overall_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["overall_rating"].ToString());
                res.ValueRating = dataReader["value_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["value_rating"].ToString());
                lisRest.Add(res);

            }
            connection.Close();
            return lisRest;
        }
        public List<Restaurante> GetAllRestaurantesByName(String Name)
        {
            List<Restaurante> lisRest = new List<Restaurante>();
            Restaurante res;
            if (!connection.Ping())
            {
                connection.Open();
            }
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM restaurantes where nombre='" +Name +"'", connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                res = new Restaurante();
                res.ID = Convert.ToInt32(dataReader["id"]);
                res.Nombre = dataReader["nombre"].ToString();
                res.Telefono = dataReader["telefono"].ToString();
                res.Mapa = dataReader["mapa"].ToString();
                res.Email = dataReader["email"].ToString();
                res.Chef = dataReader["chef"].ToString();
                res.IdRangoPrecio = dataReader["id_rango_precio"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_rango_precio"].ToString());
                res.Horario = dataReader["horario"].ToString();
                res.Reservacion = dataReader["reservacion"] == DBNull.Value ? true : Convert.ToBoolean(dataReader["reservacion"].ToString());
                res.IdEstado = dataReader["id_estado"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_estado"].ToString());
                res.Logo = "http://img.restaurante.com.do/img/logos/" + dataReader["logo"].ToString();
                res.MenuPdf = dataReader["menu_pdf"].ToString();
                res.IdMenu = dataReader["id_menu"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_menu"].ToString());
                res.Responsable = dataReader["responsable"].ToString();
                res.PaginaWeb = dataReader["pagina_web"].ToString();
                res.Descripcion = dataReader["descripcion"].ToString();
                //  res.IdMenu = Convert.ToInt32(dataReader["id_menu"].ToString());
                res.IdTipoRestaurante = dataReader["id_tipo_restaurante"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_tipo_restaurante"].ToString());
                res.OverallRating = dataReader["overall_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["overall_rating"].ToString());
                res.ValueRating = dataReader["value_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["value_rating"].ToString());
                res.CAT1 = dataReader["CAT1"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT1"].ToString());
                res.CAT2 = dataReader["CAT2"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT2"].ToString());
                res.CAT3 = dataReader["CAT3"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT3"].ToString());
                res.CAT4 = dataReader["CAT4"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT4"].ToString());
                res.CAT5 = dataReader["CAT5"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT5"].ToString());
                res.CantidadVotos = dataReader["cantidad_votos"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["cantidad_votos"].ToString());
                res.Comidas = dataReader["comidas"].ToString();
                res.SucursalId = dataReader["sucursal_id"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["sucursal_id"]);
                res.Facebook = dataReader["facebook"].ToString();
                res.Twitter = dataReader["twitter"].ToString();
                res.GooglePlus = dataReader["google_plus"].ToString();
                res.Foursquare = dataReader["foursquare"].ToString();
                lisRest.Add(res);
            }
            connection.Close();
            return lisRest;
        }
        public Restaurante GetRestauranteById(int Id) {

            Restaurante res = new Restaurante();
            if (!connection.Ping())
            {
                connection.Open();
            }
            MenuHelper m = new MenuHelper(connection);
            Rango rango;
            MySqlCommand cmd = new MySqlCommand("SELECT res.*,r.descripcion as RangoDePrecio,s.direccion,s.latitud,s.longitud FROM restaurantes  as res inner join rangos as r ON res.id_rango_precio = r.id inner join sucursales as s on res.sucursal_id = s.id  where res.id =" + Id.ToString(), connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                res = new Restaurante();
                rango = new Rango();
                rango.Descripcion = dataReader["RangoDePrecio"].ToString();
                res.ID = Convert.ToInt32(dataReader["id"]);
                res.Nombre = dataReader["nombre"].ToString();
                res.Telefono = dataReader["telefono"].ToString();
                res.Mapa = dataReader["mapa"].ToString();
                res.Email = dataReader["email"].ToString();
                res.Chef = dataReader["chef"].ToString();
                res.IdRangoPrecio = dataReader["id_rango_precio"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_rango_precio"].ToString());
                res.Horario = dataReader["horario"].ToString();
                res.Reservacion = dataReader["reservacion"] == DBNull.Value ? true : Convert.ToBoolean(dataReader["reservacion"].ToString());
                res.IdEstado = dataReader["id_estado"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_estado"].ToString());
                res.Logo = "http://img.restaurante.com.do/img/logos/"+ dataReader["logo"].ToString();
                res.MenuPdf = dataReader["menu_pdf"].ToString();
                res.IdMenu = dataReader["id_menu"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_menu"].ToString());
                res.Responsable = dataReader["responsable"].ToString(); 
                res.PaginaWeb = dataReader["pagina_web"].ToString();
                res.Descripcion = dataReader["descripcion"].ToString();
                //  res.IdMenu = Convert.ToInt32(dataReader["id_menu"].ToString());
                res.IdTipoRestaurante = dataReader["id_tipo_restaurante"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id_tipo_restaurante"].ToString());
                res.OverallRating = dataReader["overall_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["overall_rating"].ToString());
                res.ValueRating = dataReader["value_rating"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["value_rating"].ToString());
                res.CAT1 = dataReader["CAT1"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT1"].ToString());
                res.CAT2 = dataReader["CAT2"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT2"].ToString());
                res.CAT3 = dataReader["CAT3"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT3"].ToString());
                res.CAT4 = dataReader["CAT4"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT4"].ToString());
                res.CAT5 = dataReader["CAT5"] == DBNull.Value ? 0 : Convert.ToDecimal(dataReader["CAT5"].ToString());
                res.CantidadVotos = dataReader["cantidad_votos"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["cantidad_votos"].ToString());
                res.Comidas = dataReader["comidas"].ToString();
                res.SucursalId = dataReader["sucursal_id"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["sucursal_id"]);
                res.Facebook = dataReader["facebook"].ToString();
                res.Twitter = dataReader["twitter"].ToString();
                res.GooglePlus = dataReader["google_plus"].ToString();
                res.Foursquare = dataReader["foursquare"].ToString();
                res.lon =  dataReader["longitud"] == DBNull.Value ? 0 : Convert.ToDouble(dataReader["longitud"]);
                res.lat = dataReader["latitud"] == DBNull.Value ? 0 : Convert.ToDouble(dataReader["latitud"]);
                res.direccion = dataReader["direccion"].ToString();
            }
            dataReader.Close();
            res.Menu = m.GetMenubyId(res.IdMenu);
            connection.Close();
            res.Sucursal = GetSucursales(res.ID);
            return res;
        }
        public List<Foto> GetImageRestaurantes(long idRestaurant) {
            List<Foto> fotos = new List<Foto>();
            Foto foto;
            if (!connection.Ping())
            {
                connection.Open();
            }
            MenuHelper m = new MenuHelper(connection);
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM fotos where id_restaurante =" + idRestaurant.ToString(), connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                foto = new Foto();
                foto.Comentario = dataReader["comentario"].ToString();
                foto.Url = "http://restaurante.com.do/img/rest/normal/" + dataReader["url"].ToString();
                foto.Id = Convert.ToInt32(dataReader["id"].ToString());
                foto.IdRestaurante = Convert.ToInt32(dataReader["id_restaurante"].ToString());
                fotos.Add(foto);
            }
            connection.Close();
            return fotos;
        }
        public List<Sucursal> GetSucursales(long idRestaurant) {
            List<Sucursal> listSuc = new List<Sucursal>();
            connection.Open();
            Sucursal suc;
            MySqlCommand cmd = new MySqlCommand("select * from sucursales where id_restaurante ="+ idRestaurant.ToString(), connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                suc = new Sucursal();
                suc.CapacidadPersonas = dataReader["capacidad_personas"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["capacidad_personas"].ToString());             
                suc.Direccion = dataReader["direccion"].ToString();
                suc.Id = dataReader["id"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["id"].ToString());
                suc.Longitud = dataReader["longitud"] == DBNull.Value ? 0 : Convert.ToDouble(dataReader["longitud"]);
                suc.Latitud = dataReader["latitud"] == DBNull.Value ? 0 : Convert.ToDouble(dataReader["latitud"]);
                suc.Telefono = dataReader["telefono"].ToString();
                listSuc.Add(suc);

            }
            connection.Close();
            return listSuc;
        }
      
    }
}
