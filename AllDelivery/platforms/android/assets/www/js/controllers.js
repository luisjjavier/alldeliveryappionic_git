Number.prototype.formatMoney = function(c, d, t){
	var n = this,
		c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
var app  = angular.module('starter.controllers', ['ionic']);

app.directive('ngEnter', function () {
	return function (scope, element, attrs) {
		element.bind("keydown keypress", function (event) {
			if (event.which === 13) {
				scope.$apply(function () {
					scope.$eval(attrs.ngEnter, {
						'event': event
					});
				});

				event.preventDefault();
			}
		});
	};
});


app.directive('onValidSubmit', ['$parse', '$timeout', function($parse, $timeout) {
	return {
		require: '^form',
		restrict: 'A',
		link: function(scope, element, attrs, form) {
			form.$submitted = false;
			var fn = $parse(attrs.onValidSubmit);
			element.on('submit', function(event) {
				scope.$apply(function() {
					element.addClass('ng-submitted');
					form.$submitted = true;
					if (form.$valid) {
						if (typeof fn === 'function') {
							fn(scope, {$event: event});
						}
					}
				});
			});
		}
	}

}])
	.directive('validated', ['$parse', function($parse) {
		return {
			restrict: 'AEC',
			require: '^form',
			link: function(scope, element, attrs, form) {
				var inputs = element.find("*");
				for(var i = 0; i < inputs.length; i++) {
					(function(input){
						var attributes = input.attributes;
						if (attributes.getNamedItem('ng-model') != void 0 && attributes.getNamedItem('name') != void 0) {
							var field = form[attributes.name.value];
							if (field != void 0) {
								scope.$watch(function() {
									return form.$submitted + "_" + field.$valid;
								}, function() {
									if (form.$submitted != true) return;
									var inp = angular.element(input);
									if (inp.hasClass('ng-invalid')) {
										element.removeClass('has-success');
										element.addClass('has-error');
									} else {
										element.removeClass('has-error').addClass('has-success');
									}
								});
							}
						}
					})(inputs[i]);
				}
			}
		}
	}])
;
//Services/
var rutaBase = "http://45.40.135.73:7000/api/Alldelivery";
var rutaUsuarios = "http://45.40.135.73:7000/api/UsersAllDelivery";
var rutaBaseSectores = "http://45.40.135.73:7000/api";
app.controller('AppCtrl', function($scope,$state,$window,$timeout,ordenService){
	$scope.count=0;
	var restaurantes = ordenService.all();
	for (var i =0; i<restaurantes.length; i++){
		$scope.count+=   restaurantes[i].ordenes.length;
	} ;
	if (window.localStorage.getItem("isGuest") == "true"){
		console.log("true");
		$scope.isGuestUser = true;
	}else{
		$scope.isGuestUser = false;
		console.log("false");
	}
	$scope.init= function () {
		if (window.localStorage.getItem("isGuest") == "true"){

			$scope.isGuestUser = true;
		}else{
			$scope.isGuestUser = false;

		}
	}
	$scope.activeUsuarios = 'active';
	$scope.MyOrder= function()
	{
		$state.go('app.orden');
	};

	$scope.logout = function ()
	{
		localStorage.removeItem("access_token");
		localStorage.removeItem("token");
		window.localStorage.setItem('isGuest',true);
		//window.location.href = window.location.origin;
		$state.transitionTo('login', {}, {
			reload: true,
			inherit: false,
			notify: true
		});
	}

	$scope.ShowSearch = function ()
	{
		$scope.searchGeneral = !$scope.searchGeneral;
		$scope.search='';
		$timeout(function() {
			var element = $window.document.getElementById("search");
			if(element)
				element.focus();
		});


	}

});
app.controller("BebidasCtrl", function ($scope,$ionicModal,$ionicPlatform,restaurantesService,utilsService){

	$scope.icon = $ionicPlatform.is("ios") ? "spinner-ios" : "spinner-android";
	$scope.buscando = 0;
	$scope.tamano = 1;
	$scope.$parent.$parent.hasMenu = false;
	$scope.$parent.$parent.hasMenu = false;
	restaurantesService.getBebidas().then(function(response){

		$scope.restauresArray = response.data;
		$scope.buscando = 1;
		$scope.tamano = response.data.length;

	},function(error)
	{
		console.log(error);
		$scope.restauranteArray=  [];
		$scope.buscando = 1
		$scope.tamano = 0;
	});
	$scope.hideKeyBoard = function () {
		$ionicPlatform.ready(function(){
			cordova.plugins.Keyboard.close();
		});
	}
	$ionicModal.fromTemplateUrl('templates/search_filter.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});

	// Triggered in the login modal to close it
	$scope.closeFilter = function() {
		$scope.modal.hide();
	};

	// Open the login modal
	$scope.filter = function() {
		$scope.groups = [
			{
				name: 'Facilidades',
				icon: 'ion-information-circled',
				items: []
			},
			{
				name: 'Tipos de comida',
				icon: 'ion-fork',
				items: []
			},
			{
				name: 'Rango de precio',
				icon: 'ion-social-usd',
				items: [
					{
						Descripcion:"100/500",
						isCheck : false
					},
					{
						Descripcion:"500/1000",
						isCheck : false
					},
					{
						Descripcion:"1000/2000"
						,isCheck : false
					}
				]
			}
		];
		utilsService.all().then(function(response){
			response.data.Facilidades.map(function(e){
				e.isCheck =false;
				return e;
			});
			response.data.TiposDeComidas.map(function(e){
				e.isCheck =false;
				return e;
			});
			$scope.groups[0].items=response.data.Facilidades;
			$scope.groups[1].items=response.data.TiposDeComidas;
			$scope.cuidad = response.data.Ciudades;
			$scope.selectedValue = response.data.Ciudades[42] ;
			$scope.modal.show();
		},function(error)
		{
			$scope.groups[0].items=[];
			$scope.groups[1].items=[];
		});

	};

	$scope.buscarAvazando = function(groups,ciudad)
	{
		$scope.restauresArray= undefined;
		$scope.tamano = 1;
		$scope.buscando = 0;
		var data = {
			"Ciudad" : ciudad,
			"Facilidades" : [],
			"TiposDeComida" : [],
			"RangoDePrecio" : []
		}
		for(var i = 0; i < groups[0].items.length ; i++)
		{

			if(groups[0].items[i].isCheck){
				data.Facilidades.push(groups[0].items[i].Id);
			}

		}
		for(var i = 0; i < groups[1].items.length ; i++)
		{
			if(groups[1].items[i].isCheck){
				data.Facilidades.push(groups[1].items[i].Id);
			}

		}
		for(var i = 0; i < groups[2].items.length ; i++)
		{
			if(groups[2].items[i].isCheck){
				data.Facilidades.push(groups[2].items[i].Descripcion);
			}

		}


		restaurantesService.advaceSearch(data).then(function(res){

			$scope.restauresArray = res.data;
			$scope.buscando = 1;
			$scope.tamano = res.data.length
		});

		$scope.modal.hide();
	};


	/*
	 * if given group is the selected group, deselect it
	 * else, select the given group
	 */
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
});
app.controller("RegistroCtrl",function ($scope,$state,$ionicLoading,userService, $http){
	$scope.user  = {
		name :"",
		password: "",
		email : "",
		phone : "",
		username : "",
		lastName : ""

	}
	$scope.register = function (user)
	{

		if (!user.username || !user.email || !user.password || !user.phone){
			alert("Debe completar los campos");

			return;
		}
		$ionicLoading.show({
			content: 'Guardando',
			showBackdrop: false
		});
		var  usr  =	userService.newUser(user);
		console.log(angular.toJson(usr));
		userService.createUser(usr).then(function(data){
			$ionicLoading.hide();
			window.localStorage.setItem('access_token', angular.toJson(data.data));
			window.localStorage.setItem("isGuest","false");
			$state.go('app.restaurantes');
		},function(error){
			$ionicLoading.hide();
			console.log(error);
		});
	}
	console.log(window.localStorage.token);
		if (window.localStorage.hasOwnProperty("token") === true) {
			$ionicLoading.show({
				content: 'Guardando',
				showBackdrop: false
			});
			$http.get("https://graph.facebook.com/v2.2/me", {
				params: {
					access_token: window.localStorage.token,
					fields: "id,email,name,gender",
					format: "json"
				}
			}).then(function (result) {
				$scope.user.name = result.data.name;
				$scope.user.email = result.data.email;
				$scope.user.username = result.data.email;

				$ionicLoading.hide()
				console.log(result.data);
				console.log($scope.user);
			}, function (error) {
				console.log(error);
				$ionicLoading.hide();
			});
		}

});
app.controller("LoginCtrl", function($scope,$state,$ionicHistory,$ionicLoading,userService,$cordovaOauth){

	$ionicHistory.nextViewOptions({
		disableBack: true
	});
	$scope.login   = function(model){
		$ionicLoading.show({
			content: 'Guardando',
			showBackdrop: false
		});
		userService.login(model.username,model.password).then(function(res){
			window.localStorage.setItem('isGuest',"false");
			if (res.data) {
				console.log(res.data);
				$ionicLoading.hide();
				window.localStorage.setItem('access_token', angular.toJson(res.data));
				$state.go('app.restaurantes');
			}
			else
			{
				$ionicLoading.hide();
				alert("Usuario o contraseņa invalidos");
			}
		},function(error){
			$ionicLoading.hide();
			alert("Usuario o contraseņa invalidos");
		});
	}
	$ionicHistory.clearCache();
	$scope.signInFacebook= function()
	{
			$cordovaOauth.facebook("385346264994781", ["email", "read_stream", "user_website"]).then(function(result) {
				window.localStorage.setItem('token', result.access_token);
				//window.localStorage.setItem('isGuest', false);
				console.log(result);
				$state.go('registro');
				//$location.path("/profile");

			}, function(error) {

				console.log(error);
			});
	};
	$scope.signInGuest = function ()
	{
		window.localStorage.setItem('isGuest',true);

		if (window.localStorage.getItem("isGuest") == "true"){
			console.log("true");
			$scope.isGuestUser = true;
		}else{
			$scope.isGuestUser = false;
			console.log("false");
		}
		$state.go('app.restaurantes');
	};
$scope.register = function (user)
{

	if (!user.username || !user.email || !user.password || user.phone){
		alert("Debe completar los campos");

		return;
	}
	$ionicLoading.show({
		content: 'Guardando',
		showBackdrop: false
	});
	var  usr  =	userService.newUser(user);
	console.log(angular.toJson(usr));
	userService.createUser(usr).then(function(data){
		$ionicLoading.hide();
		console.log(data);
		window.localStorage.setItem("isGuest",false);
		$state.go('app.restaurantes');
	},function(error){
		$ionicLoading.hide();
		console.log(error);
	});
}
$scope.initRegister  = function() {
	if (window.localStorage.hasOwnProperty("token") === true) {
		$http.get("https://graph.facebook.com/v2.2/me", {
			params: {
				access_token: window.localStorage.token,
				fields: "id,email,name,gender",
				format: "json"
			}
		}).then(function (result) {

			console.log(result.data);
		}, function (error) {
			console.log(error);
		});
	}
}
});

app.controller("OrdenCtrl", function($scope,$state,$ionicPopup, $http,ordenService,facturaService){
	$scope.$parent.$parent.hasMenu = false;
	$scope.restauranteArray = ordenService.all();
	$scope.factura = facturaService.getFacturaValue();

	if (window.localStorage.getItem("isGuest") === "true"){
		$scope.isGuestUser = true;
	}else{
		$scope.isGuestUser = false;
	}

	$scope.PagarCard= function(factura)
	{
		factura.status = 2 ;

		if(window.localStorage.hasOwnProperty("access_token") === true) {
				$state.go('app.enviarPedido',{factura : factura, status : 2});

		} else {
			alert("Debe iniciar sesion para ordenar");
		}
	};
	$scope.PagarCash= function(factura)
	{
		factura.status = 1 ;
		if(window.localStorage.hasOwnProperty("access_token") === true) {
			$state.go('app.enviarPedido',{factura : factura,status : 1});

		} else {
			alert("Debe iniciar sesion para ordenar");
		}
	};

	$scope.DeleteLists = function () {

			var confirmDeleteAll = $ionicPopup.confirm({
				title: '<i class="ion ion-information-circled"></i> Borrar toda la lista',
				template: 'Esta seguro que quiere borrar toda la lista del pedido?'
			});
		confirmDeleteAll.then(function(res) {
				if(res) {
					ordenService.deleteAllOrden();
					$scope.restauranteArray = ordenService.all();
					$scope.factura = facturaService.getFacturaValue();
					$scope.$parent.$parent.count=0;
					var restaurantes = ordenService.all();
					for (var i =0; i<restaurantes.length; i++){
						$scope.$parent.$parent.count +=   restaurantes[i].ordenes.length;
					} ;
				}
			});



	}
	$scope.deleteItem = function (restauranteId, platoId)
	{
		var confirmDeleteOne = $ionicPopup.confirm({
			title: '<i class="ion ion-information-circled"></i> Borrar Plato ',
			template: 'Esta seguro que quiere borrar este Plato?'
		});
		confirmDeleteOne.then(function(res) {
			if(res) {
				ordenService.deleteOrden(restauranteId,platoId);
				$scope.restauranteArray = ordenService.all();
				$scope.factura = facturaService.getFacturaValue();
				$scope.$parent.$parent.count=0;
				var restaurantes = ordenService.all();
				for (var i =0; i<restaurantes.length; i++){
					$scope.$parent.$parent.count +=   restaurantes[i].ordenes.length;
				} ;
			}
		});
	}
});
app.controller("EnviarPedidoCtrl", function($scope,$state,$ionicHistory,$http,$stateParams,$ionicLoading,facturaService,ordenService,utilsService){
	$scope.$parent.$parent.hasMenu = false;
	$scope.status= $stateParams.factura.status;
	 var user = angular.fromJson( window.localStorage.access_token);
	console.log(user);
	$scope.user = user;
	$scope.telefono  = user.UserTel;
	console.log($stateParams)
	utilsService.getSectores().then(function(res){
		$scope.sectores = res.data;
		$scope.selectedValue= res.data[0];

	});

$scope.factura = facturaService.getFacturaValue();

	$scope.enviarPedido = function (factura,direccion,telefono,sector,devuelta,status) {
		ordenService.checkTime().then(function (res){

			if (res.data){
				if (direccion && telefono && devuelta){



					var factura  = facturaService.getFacturaValue();
					if (devuelta < factura.total){
						alert("La devuelta tiene que ser mayor al monto total");
						return;
					}
					$ionicLoading.show({
						content: 'Getting current location...',
						showBackdrop: false
					});
					var ordenes =  ordenService.all();
					var usuario  = $scope.user;
					var gcm = angular.fromJson( window.localStorage.getItem("idGCM"));
					factura.devueltaDe = devuelta;
					var envio = {
						"UserId": user.Id,
						"Amount": factura.total,
						"ITBIS": factura.itbis,
						"date": "2015-03-19 14:48:22",
						"Status": status,
						"Modified_by": 1,
						"cash_back": devuelta,
						"Confirmation_notes": "holaa",
						"Fee": factura.fee,
						"rnc": "",
						"companyName": "",
						"Address" : direccion,
						"Sector" : sector.Descripcion,
						"Name" : user.user_nicename,
						"Phone" : telefono,
						"reference" : "atras de ti",
						"ten_percent_law" : factura.ley,
						"ordenes" :[],
						"IdCel" : gcm.token


					};



					var orden = ordenes.map(function(obj){
						return {
							RestId : obj.restauranteId,
							Platos : obj.ordenes.map(function(e){
								return {
									Code : "RNA",
									Quantity : e.cantidad,
									Name : e.nombre,
									Description : e.descripcion,
									Coment : e.comentario,
									Price : e.precio,
									"OrderRestaurant": 0
								}
							})

						}
					});
					console.log(gcm);
					console.log(envio);
					envio.ordenes = orden;
					console.log(angular.toJson(envio));

					ordenService.sendOrden(envio).then(function	(){
						//console.log(angular.toJson(envio));
						ordenService.deleteAllOrden();
						alert("Se a enviado satisfactoriamente!!");
						$scope.$parent.$parent.count=0;
						var restaurantes = ordenService.all();
						for (var i =0; i<restaurantes.length; i++){
							$scope.$parent.$parent.count +=   restaurantes[i].ordenes.length;
						} ;
						$ionicHistory.nextViewOptions({
							disableBack: true
						});
						$ionicLoading.hide();
						$state.go("app.restaurantes");

					},function(error){
						alert("Ha ocurrido un error enviando el pedido,por favor intentelo mas tarde.");
					});


				}
				else {
					alert("Debe completar todos los campos");
				}

			}else{
				console.log(res.data);
				alert ("Solo se puden enviar pedidos de :  LUNES A VIERNES  DE 9:00 AM A 10:40 PM SABADOS Y DOMINGOS  DE 11:00 AM A 10:40 PM");
				return false;
			}
		},function(error){
			alert ("Solo se puden enviar pedidos de :  LUNES A VIERNES  DE 9:00 AM A 10:40 PM SABADOS Y DOMINGOS  DE 11:00 AM A 10:40 PM");
			return false;
		});

	}


/*	if(window.localStorage.hasOwnProperty("access_token") === true) {
		$http.get("https://graph.facebook.com/v2.2/me", { params: { access_token: window.localStorage.access_token, fields: "id,email,name,gender,location,website,picture,relationship_status", format: "json" }}).then(function(result) {
			$scope.profileData = result.data;
			console.log(result.data);
		}, function(error) {
			alert("There was a problem getting your profile.  Check the logs for details.");
			console.log(error);
		});
	} else {
		alert("Not signed in");
		$location.path("/login");
	}*/
});

app.controller("RestauratesCtrl",function($scope,$ionicModal,$ionicPlatform,restaurantesService,utilsService){
	$scope.icon = $ionicPlatform.is("ios") ? "spinner-ios" : "spinner-android";
	$scope.buscando = 0;
	$scope.tamano = 1;
	$scope.$parent.$parent.hasMenu = false;
	$scope.$parent.$parent.hasMenu = false;
	restaurantesService.all().then(function(response){

		$scope.restauresArray = response.data;
		$scope.buscando = 1;
		$scope.tamano = response.data.length;

	},function(error)
	{
		console.log(error);
		$scope.restauranteArray=  [];
		$scope.buscando = 1
		$scope.tamano = 0;
	});
	$scope.hideKeyBoard = function () {
		$ionicPlatform.ready(function(){
			cordova.plugins.Keyboard.close();
		});
	}
	$ionicModal.fromTemplateUrl('templates/search_filter.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});

	// Triggered in the login modal to close it
	$scope.closeFilter = function() {
		$scope.modal.hide();
	};

	// Open the login modal
	$scope.filter = function() {
		$scope.groups = [
			{
				name: 'Facilidades',
				icon: 'ion-information-circled',
				items: []
			},
			{
				name: 'Tipos de comida',
				icon: 'ion-fork',
				items: []
			},
			{
				name: 'Rango de precio',
				icon: 'ion-social-usd',
				items: [
					{
						Descripcion:"100/500",
						isCheck : false
					},
					{
						Descripcion:"500/1000",
						isCheck : false
					},
					{
						Descripcion:"1000/2000"
						,isCheck : false
					}
				]
			}
		];
		utilsService.all().then(function(response){
			response.data.Facilidades.map(function(e){
				e.isCheck =false;
				return e;
			});
			response.data.TiposDeComidas.map(function(e){
				e.isCheck =false;
				return e;
			});
			$scope.groups[0].items=response.data.Facilidades;
			$scope.groups[1].items=response.data.TiposDeComidas;
			$scope.cuidad = response.data.Ciudades;
			$scope.selectedValue = response.data.Ciudades[42] ;
			$scope.modal.show();
		},function(error)
		{
			$scope.groups[0].items=[];
			$scope.groups[1].items=[];
		});

	};

	$scope.buscarAvazando = function(groups,ciudad)
	{
		$scope.restauresArray= undefined;
		$scope.tamano = 1;
		$scope.buscando = 0;
		var data = {
			"Ciudad" : ciudad,
			"Facilidades" : [],
			"TiposDeComida" : [],
			"RangoDePrecio" : []
 		}
		for(var i = 0; i < groups[0].items.length ; i++)
		{

			if(groups[0].items[i].isCheck){
				data.Facilidades.push(groups[0].items[i].Id);
			}

		}
		for(var i = 0; i < groups[1].items.length ; i++)
		{
			if(groups[1].items[i].isCheck){
				data.Facilidades.push(groups[1].items[i].Id);
			}

		}
		for(var i = 0; i < groups[2].items.length ; i++)
		{
			if(groups[2].items[i].isCheck){
				data.Facilidades.push(groups[2].items[i].Descripcion);
			}

		}


		restaurantesService.advaceSearch(data).then(function(res){

			$scope.restauresArray = res.data;
			$scope.buscando = 1;
			$scope.tamano = res.data.length
		});

		$scope.modal.hide();
	};


	/*
	 * if given group is the selected group, deselect it
	 * else, select the given group
	 */
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
});


app.controller("RestaurateCtrl",function($scope,$ionicModal,$ionicPopup,$stateParams,restaurantesService,$ionicPlatform,ordenService){
	$scope.icon = $ionicPlatform.is("ios") ? "spinner-ios" : "spinner-android";
	$scope.hasMenu = true;

	if(window.cordova && window.cordova.plugins.diagnostic)
	{
	var dic = cordova.plugins.diagnostic;
	dic.isLocationEnabledSetting(function (success) {

			if (success === 1){
				$scope.locationOn = true;
			}
			else {
				$scope.locationOn = false;
				console.log($scope.locationOn );
			}

		}, function(error){

		}
	);
	}
	else{
		$scope.locationOn = true;
		console.log($scope.locationOn  + "algo mas")
	}


	$scope.$parent.$parent.search="";
	$scope.$parent.$parent.searchGeneral=false;
	$scope.$parent.$parent.hasMenu = true;






	$scope.showImage = function(index) {

		$scope.imageSrc = index;
		console.log(index);

		$scope.openModal();
	}

	$scope.popup = function(plato,restaurante) {
		$scope.plato = plato;
		$scope.plato.restauranteNombre = restaurante.Nombre;
		$scope.plato.restauranteId  = restaurante.ID;
		$scope.plato.cantidad  = 1;
		$scope.plato.comentario = "";


		var pop =  $ionicPopup.show({
			templateUrl: "templates/popup_pedido.html",
			title: '<i class="ion ion-pricetag"></i> '+ $scope.plato.Nombre+'',
			scope: $scope,

			buttons: [
				{
					text: '<i class="ion ion-close-round"></i> Cancelar',
					type: 'button-assertive'
				},
				{
					text: '<i class="ion ion-plus-round"></i> <b> Agregar</b>',
					type: 'button-positive',
					onTap: function (e) {
						return $scope.plato;
					}
				}
			]
		});
		pop.then(function (res) {
		if (res) {
			if (ordenService.canAddOrden()) {
			var restaurante = ordenService.newOrden(res.restauranteNombre, res.restauranteId, res);
			ordenService.save(restaurante);
			console.log(ordenService.all());
				$scope.$parent.$parent.count=0;
				var restaurantes = ordenService.all();
				for (var i =0; i<restaurantes.length; i++){
					$scope.$parent.$parent.count +=   restaurantes[i].ordenes.length;
				} ;
				}else
			{
				alert("Solo se puede perdir de 3 restaurantes a la vez.");
			}
		}
		});




	};

 restaurantesService.get($stateParams.restauranteId).then(function(response){
	 $scope.restaurante = response.data;
	 if($scope.restaurante.Menu.CategoriaMenu[0])
	 {
		 $scope.hasMenu = true;
	 }else
	 {
		 s$cope.hasMenu = false;
	 }
 }, function (error) {
	 $scope.restaurante = error;
	 $scope.hasMenu = false;
 });
	$scope.SumProduct= function()
	{
		$scope.plato.cantidad+=1;
	};
	$scope.SubtractProduct= function()
	{
		if($scope.plato.cantidad > 1){
			$scope.plato.cantidad -= 1;
		}

	};
	$scope.popupFacilities = function(facilities) {
		$scope.facilities = facilities;
		$ionicModal.fromTemplateUrl('templates/facilities.html', {
			scope: $scope
		}).then(function(modal) {
			console.log(modal)
			$scope.modal = modal;
			$scope.modal.show();
		});

		// Triggered in the login modal to close it
		$scope.closeFacilities = function() {
			$scope.modal.hide();
		};


	};
});

app.controller("OfertasCtrl",function($scope,$http,$ionicPopup,ofertasService,ordenService,$ionicPlatform){
	$scope.icon = $ionicPlatform.is("ios") ? "spinner-ios" : "spinner-android";
	$scope.$parent.$parent.hasMenu = false;
	$scope.SumProduct= function()
	{
		$scope.plato.cantidad+=1;
	};
	$scope.SubtractProduct= function()
	{
		if($scope.plato.cantidad > 1){
			$scope.plato.cantidad -= 1;
		}

	};
	$scope.addCard = function(of)
	{
console.log(of);

		$scope.plato = of;
		$scope.plato.restauranteNombre = of.Restaurante;
		$scope.plato.restauranteId  = of.IdRestaurante;
		$scope.plato.cantidad  = 1;
		$scope.plato.comentario = "Oferta-"+of.menu;
		$scope.plato.Precio = of.precioDescuento;
		$scope.plato.Descripcion = of.Plato;
		$scope.plato.Nombre  = of.menu;
		var pop =  $ionicPopup.show({
			templateUrl: "templates/popup_pedido.html",
			title: '<i class="ion ion-pricetag"></i> '+ $scope.plato.Nombre+'',
			scope: $scope,

			buttons: [
				{
					text: '<i class="ion ion-close-round"></i> Cancelar',
					type: 'button-assertive'
				},
				{
					text: '<i class="ion ion-plus-round"></i> <b> Agregar</b>',
					type: 'button-positive',
					onTap: function (e) {
						return $scope.plato;
					}
				}
			]
		});
		pop.then(function (res) {
			if (res) {
				if(ordenService.canAddOrden()){
				var restaurante = ordenService.newOrden(res.restauranteNombre, res.restauranteId, res);
				ordenService.save(restaurante);
				console.log(ordenService.all());
					$scope.$parent.$parent.count=0;
					var restaurantes = ordenService.all();
					for (var i =0; i<restaurantes.length; i++){
						$scope.$parent.$parent.count +=   restaurantes[i].ordenes.length;
					} ;
			}else
				{
					alert("Solo se puede perdir de 3 restaurantes a la vez.");
				}
			}
		});
		console.log(of);
	}
	ofertasService.all().then(function(response){

		$scope.ofertasArray = response.data;
		console.log(response.data);

	},function(error)
	{
		console.log(error);
	});
});
app.controller("UsuarioCtrl",function($scope,$ionicLoading,userService){
	$scope.$parent.$parent.hasMenu = false;
	$scope.init = function() {
		if(window.localStorage.hasOwnProperty("access_token") === true) {
			var user = angular.fromJson( window.localStorage.access_token);
			console.log(user);
			$scope.user= user;
		}
		};
	$scope.update = function(user){
		console.log(user);
		$ionicLoading.show({
			content: 'Getting current location...',
			showBackdrop: false
		});
		console.log(user);
		userService.updateUser(user).then(function(res){
			alert("Se guardaron los cambios.");
			console.log(res);
			window.localStorage.setItem('access_token', angular.toJson(user));
			$ionicLoading.hide();
		},function(error){
			console.log(error);
			alert("No se pudieron guardar los cambios, Por favor intentelo mas tarde.");
			$ionicLoading.hide();
		});
		}
});

app.controller ("PasswordCtrl",function ($scope,userService,$ionicLoading){
	$scope.$parent.$parent.hasMenu = false;
	$scope.changePassword = function(password){

	if(password.new != password.repeatNew){
		console.log(password)
		alert("Las contrasenas nuevas tienen que coincidir");
		return;
	}var user ={};
		if(window.localStorage.hasOwnProperty("access_token") === true) {
			user = angular.fromJson( window.localStorage.access_token);
			console.log(user);

		}
		$ionicLoading.show({
			content: 'Getting current location...',
			showBackdrop: false
		});
		password.id=user.Id;
		userService.changePassword(password).then (function (res) {
			alert("El cambio se realizo con exito");
			$ionicLoading.hide();
		},function (error){
			alert("No se pudo cambiar la contrasena, intente de nuevo mas tarde.");
			$ionicLoading.hide();
		});

	}
});
app.controller("HistorialCtrl",function($scope,ordenService){
	$scope.tamano = true;
	$scope.empty = false;
	var user = angular.fromJson( window.localStorage.access_token);
		ordenService.getPending(user.Id).then(function(res){
			$scope.historial = res.data;
			console.log(res.data);
			if(res.data.length == 0){
				$scope.empty = true;
			}
			$scope.tamano= false;
		},function(error){
			console.log(error);
			$scope.error= error;
			$scope.tamano= false;
		});
});
//factorias

app.factory('restaurantesService', function($http) {

  return {
  			all : function()
  			{
				return $http.get(rutaBase);
  			},
  			get : function(restaurantesId)
  			{
  				return $http.get(rutaBase + "/"+ restaurantesId);
  			},
	  getBebidas : function(){
		  return $http.get("http://45.40.135.73:7000/api/bebidas");
	  }
  }
});
app.factory('ofertasService', function($http) {

	return {
		all : function()
		{
			return $http.get(rutaBase+"?Type=Ofertas");
		}
	}
});

app.factory('userService', function ($http){

	return {
		newUser : function (user)
		{
			var usr = {
				"Id": 0,
				"user_login": user.email,
				"user_pass": user.password,
				"user_nicename": user.name,
				"user_email":  user.email,
				"user_url": "",
				"user_registered": "2015-06-10T18:53:23.0494449-07:00",
				"user_activation_key": "aaa",
				"user_status": "Activo",
				"display_name": user.name + " " + user.lastName,
				"UserTel" : user.phone
			}
			return usr;
		},
		createUser : function (user ) {

			return $http.post(rutaUsuarios, user);
		},
		login : function(email,password){
			return $http.get(rutaUsuarios+"?username="+email+"&pass="+password);
		},
		updateUser : function (user)
		{
			return $http.put(rutaUsuarios,user);
		},
		changePassword  : function(password){
			return $http.get(rutaUsuarios+"/"+password.id+"?oldPassword="+password.old+"&NewPass="+password.new);
		}
	}
});
app.factory('ordenService', function ($http) {

	return {
		save : function(restaurante)
		{
			var restauranteString = window.localStorage['pedidos.'+restaurante.restauranteId];
			if(restauranteString) {

				var restauranteGuardado  =  angular.fromJson(restauranteString);
						restauranteGuardado.ordenes.push(restaurante.plato);


				window.localStorage.setItem('pedidos.'+restaurante.restauranteId,angular.toJson(restauranteGuardado));

			}else
			{

				var ordenNueva  = [];
				ordenNueva.push(restaurante.plato);
				delete restaurante["plato"];
				restaurante.ordenes = ordenNueva;
				window.localStorage['pedidos.'+restaurante.restauranteId] = angular.toJson(restaurante);
			}

		},
		all: function() {
			var restaurantes = [];
			Object.keys(localStorage)
				.forEach(function(key){
					if (/^(pedidos.)/.test(key)) {

						restaurantes.push(angular.fromJson (window.localStorage[key]));
					}
				});
			return restaurantes;
		},
		deleteOrden : function (restauranteId, platoId) {
			var restauranteString = angular.fromJson(window.localStorage['pedidos.' + restauranteId]);
			if(restauranteString)
			{

				for(i = 0; i < restauranteString.ordenes.length; i++){
					if( restauranteString.ordenes[i].Id === platoId){
						restauranteString.ordenes.splice(i,1)
						break;
					}
				}
			}
			if(restauranteString.ordenes.length!=0 ) {
				window.localStorage['pedidos.' + restauranteId] = angular.toJson(restauranteString);
			}else{
				window.localStorage.removeItem("pedidos."+restauranteId);
			}
		},
		deleteAllOrden : function () {
			Object.keys(localStorage)
				.forEach(function(key){
					if (/^(pedidos.)/.test(key)) {

						window.localStorage.removeItem(key);
					}
				});
		},
		newOrden: function(restauranteNombre, restauranteId,plato) {
			// Add a new project
			return {
				restauranteNombre: restauranteNombre,
				restauranteId : restauranteId,
				plato : {
					Id : plato.Id,
					descripcion : plato.Descripcion,
					precio : plato.Precio,
					cantidad : plato.cantidad,
					nombre : plato.Nombre,
					comentario : plato.comentario

				}
			};
		},
		sendOrden : function(object)
		{
			return $http.post(rutaBase + "/Utils",object);
		},
		canAddOrden : function(){
			return this.all().length > 2 ? false :true;
		},
		checkTime : function (){
			return $http.get(rutaBaseSectores+"/AllDeliveryUtils");
		},
		getPending : function(id) {
			return $http.get(rutaBaseSectores + "/AllDeliveryUtils?userid=" + id)
		}

	};
});
app.factory('facturaService', function (ordenService) {
	return {
		getFacturaValue : function () {
			var factura = {
				subTotal : 0.0,
				itbis : 0.0,
				fee : 0,
				ley : 0.0,
				total : 0.0
			};
			var restauranteArray = ordenService.all();
			for (var i  = 0; i < restauranteArray.length ; i++){
				for (var k  = 0; k < restauranteArray[i].ordenes.length ; k++){

					factura.subTotal  += (restauranteArray[i].ordenes[k].precio * restauranteArray[i].ordenes[k].cantidad  | 1);
				}
			}
			factura.fee = factura.subTotal > 0 ? 45 : 0;
			factura.ley = factura.subTotal  * 0.10;
			factura.itbis  = factura.subTotal  * 0.18;
			factura.total = factura.fee + factura.itbis + factura.ley + factura.subTotal;
			return factura;
		}
	}
});
app.factory('utilsService', function ($http) {

	return {

		all: function() {
			return $http.get(rutaBase + "/Utils");
		},
		getSectores : function () {
			return $http.get(rutaBaseSectores + "/Utils?type=sectores");
		}

	};
});
//filter




